#!/usr/bin/php7.0
<?php
echo PHP_EOL, PHP_EOL, "\t    Happy New 2018 Year to ПІ-52!!!", PHP_EOL, PHP_EOL, PHP_EOL;
draw_new_year_tree(5, 6);
usleep(90000);

function draw_new_year_tree(int $start_deep = 3, int $trias = 3){
	$spaces = array_fill(0, ($start_deep + $trias - 1) + $trias * $start_deep / 4, ' ');
	$spaces_count = count($spaces);
	$spaces_string = implode('', $spaces);
	for($i = 1, $deep = $start_deep; $i <= $trias; $i++){
		for($j = 1; $j < $deep; $j++){
			$stars = array_fill(0, $j*4, '*');
			array_walk($stars, function(&$el){
				$r = rand(16, 256);
			 	$el = "\e[48;5;{$r}m$el";
			});
			$stars[] = "\e[0m";
			echo $spaces_string, implode('', array_fill(0, ($deep - $j) * 2, ' ')), implode('', $stars), PHP_EOL;
		}
		echo $spaces_string, implode('', array_fill(0, ($deep - 1)*2 + 1, ' ')), '||', PHP_EOL;
		$spaces_string = substr($spaces_string, 0, -2);
		++$deep;		
	}
} 
